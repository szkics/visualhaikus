%% ==============================================================
%% ETDK cikk dokumentumosztály
%% Szerző: Sulyok Csaba <csaba.sulyok@gmail.com>
%% ==============================================================
%% jelenlegi verzió: 1.3.2, utolsó módosítás dátuma: 2019.05.01.
%% ==============================================================
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{etdkcikk}[2019/03/06 ETDK cikk]

% kiindulópont: article.cls
\LoadClass[a4paper,12pt,titlepage,oneside,onecolumn,final]{article}

% alapértelmezett serif betűtípus: Times New Roman
\RequirePackage{times}
% alapértelmezett monospace (URL és kódrészlet) betűtípus: newtxtt
\RequirePackage{newtxtt}
\RequirePackage{verbatim}

% margó és sortávolság beállítása
\RequirePackage{geometry}
\RequirePackage{setspace}
\geometry{a4paper, left=30mm, right=20mm, top=20mm,	bottom=20mm}
\linespread{1.4}

% fejezetenkénti első bekezdés indentálása
\RequirePackage{indentfirst}

% vonalkázás kerülése
\RequirePackage[none]{hyphenat}

% ábrák beszúrására és felcimkézésére alkalmas könyvtárak
\RequirePackage{graphicx} 
\RequirePackage{float}
\RequirePackage{wrapfig}
\RequirePackage{caption}
\RequirePackage{subcaption}

% ha ábra kerül az oldal aljára, láblécek mindig kerüljenek alá
\RequirePackage[bottom]{footmisc}

% a magyar karakterek támogatására alkalmas könyvtárak és beállítások
\RequirePackage[utf8]{inputenc}
\RequirePackage[magyar]{babel}
\selectlanguage{magyar}
\frenchspacing

% URL-ek beszúrására és linkelésére alkalmas könyvtár
\RequirePackage[unicode]{hyperref}
\RequirePackage{xcolor}
\definecolor{dark-blue}{rgb}{0.1,0.1,0.3}
\hypersetup{colorlinks, 
  urlcolor={dark-blue},
	citecolor={dark-blue},
	linkcolor={dark-blue}
}

% kódrészletek beszúrására és szintaxisszínezésére alkalmas könyvtár
\RequirePackage[newfloat=true]{minted}
% a kódrészletek ne vegyék át a másfeles sortávolságot
\setminted{baselinestretch=1}

% a következő szükséges, hogy a minted együttműködjön a magyar babel-lel
% ld. https://github.com/gpoore/minted/issues/158
\makeatletter
\minted@checkstyle{}
\renewcommand{\fps@listing}{t}
\SetupFloatingEnvironment{listing}{name=kódrészlet}
\makeatother


% könyvészet library-je - magyar.lbx file szükséges ide
\RequirePackage[strict=false]{csquotes}
\DeclareQuoteAlias{croatian}{magyar}
\RequirePackage[abbreviate=false,backend=biber,language=magyar]{biblatex}
% kisebb betűméret a hivatkozásoknak
\renewcommand*{\bibfont}{\small}

% utolsó megtekintés dátumának formázása
\DeclareFieldFormat{urldate}{%
  (\bibstring{urlseen}:\addspace\thefield{urlyear}\adddot\addspace
  \mkbibmonth{\thefield{urlmonth}}\addspace\thefield{urlday}\adddot)}


%% =========================================================
%% ETDK címoldal
%% =========================================================

\makeatletter
\RequirePackage{ifthen}
\RequirePackage{tikzpagenodes}
\RequirePackage{pgfkeys}

% konferencialeírás és alcím macro-k
\newcommand*{\confdescription}[1]{\gdef\@confdescription{#1}}
\newcommand*{\@confdescription}{\texttt{\string\confdescription} not set, please fix.}
\newcommand*{\subtitle}[1]{\gdef\@subtitle{#1}}
\newcommand*{\@subtitle}{}

% projekt- és intézménylogok macro-i opcionális méretezéssel
\pgfkeys{
  /projectlogo/.is family, /projectlogo,
  default/.style = {width = .3\linewidth},
  width/.estore in = \@projectlogowidth,
  /institutionlogo/.is family, /institutionlogo,
  default/.style = {width = .3\linewidth},
  width/.estore in = \@institutionlogowidth
}
\newcommand*{\projectlogo}[2][]{
  \pgfkeys{/projectlogo, default, #1}
  \gdef\@projectlogo{#2}
}
\newcommand*{\@projectlogo}{}
\newcommand*{\institutionlogo}[2][]{
  \pgfkeys{/institutionlogo, default, #1}
  \gdef\@institutionlogo{#2}
}
\newcommand*{\@institutionlogo}{}


% szerzők és témavezetők macro-i

\renewcommand*{\author}[2]{\gdef\@authoronename{#1} \gdef\@authoroneaffiliation{#2}}
\newcommand*{\authorone}[2]{\gdef\@authoronename{#1} \gdef\@authoroneaffiliation{#2}}
\newcommand*{\authortwo}[2]{\gdef\@authortwoname{#1} \gdef\@authortwoaffiliation{#2}}
\newcommand*{\authorthree}[2]{\gdef\@authorthreename{#1} \gdef\@authorthreeaffiliation{#2}}
\newcommand*{\authorfour}[2]{\gdef\@authorfourname{#1} \gdef\@authorfouraffiliation{#2}}
\newcommand*{\authorfive}[2]{\gdef\@authorfivename{#1} \gdef\@authorfiveaffiliation{#2}}

\newcommand*{\@authoronename}{\texttt{\string\author} not set, please fix.}
\newcommand*{\@authoroneaffiliation}{}
\newcommand*{\@authortwoname}{}
\newcommand*{\@authortwoaffiliation}{}
\newcommand*{\@authorthreename}{}
\newcommand*{\@authorthreeaffiliation}{}
\newcommand*{\@authorfourname}{}
\newcommand*{\@authorfouraffiliation}{}
\newcommand*{\@authorfivename}{}
\newcommand*{\@authorfiveaffiliation}{}

\newcommand*{\supervisor}[2]{\gdef\@supervisoronename{#1} \gdef\@supervisoroneaffiliation{#2}}
\newcommand*{\supervisorone}[2]{\gdef\@supervisoronename{#1} \gdef\@supervisoroneaffiliation{#2}}
\newcommand*{\supervisortwo}[2]{\gdef\@supervisortwoname{#1} \gdef\@supervisortwoaffiliation{#2}}
\newcommand*{\supervisorthree}[2]{\gdef\@supervisorthreename{#1} \gdef\@supervisorthreeaffiliation{#2}}
\newcommand*{\supervisorfour}[2]{\gdef\@supervisorfourname{#1} \gdef\@supervisorfouraffiliation{#2}}
\newcommand*{\supervisorfive}[2]{\gdef\@supervisorfivename{#1} \gdef\@supervisorfiveaffiliation{#2}}

\newcommand*{\@supervisoronename}{\texttt{\string\supervisor} not set, please fix.}
\newcommand*{\@supervisoroneaffiliation}{}
\newcommand*{\@supervisortwoname}{}
\newcommand*{\@supervisortwoaffiliation}{}
\newcommand*{\@supervisorthreename}{}
\newcommand*{\@supervisorthreeaffiliation}{}
\newcommand*{\@supervisorfourname}{}
\newcommand*{\@supervisorfouraffiliation}{}
\newcommand*{\@supervisorfivename}{}
\newcommand*{\@supervisorfiveaffiliation}{}


% maga a címoldal

\renewcommand*{\maketitle}{

\begin{titlepage}

{\centering\bfseries\@confdescription\unskip\strut\par}

\vfill

{\centering\LARGE\bfseries\@title\unskip\strut\par}
{\centering\Large\bfseries\@subtitle\unskip\strut\par}

\vfill

\ifthenelse{\equal{\@projectlogo}{}}{}{
  {\hfill\includegraphics[width=\@projectlogowidth]{\@projectlogo}\hfill}
}

\vfill

{\noindent\bfseries Szerzők:\par}\vspace{.2cm}
  {\noindent\bfseries\@authoronename\unskip\\}
  {\noindent\@authoroneaffiliation\unskip\par}
  \ifthenelse{\equal{\@authortwoname}{}}{}{
    {\noindent\bfseries\@authortwoname\unskip\\}
    {\noindent\@authortwoaffiliation\unskip\par}
  }
  \ifthenelse{\equal{\@authorthreename}{}}{}{
    {\noindent\bfseries\@authorthreename\unskip\\}
    {\noindent\@authorthreeaffiliation\unskip\par}
  }
  \ifthenelse{\equal{\@authorfourname}{}}{}{
    {\noindent\bfseries\@authorfourname\unskip\\}
    {\noindent\@authorfouraffiliation\unskip\par}
  }
  \ifthenelse{\equal{\@authorfivename}{}}{}{
    {\noindent\bfseries\@authorfivename\unskip\\}
    {\noindent\@authorfiveaffiliation\unskip\par}
  }

\vspace{.5cm}
{\noindent\bfseries Témavezetők:\par}\vspace{.2cm}
  {\noindent\bfseries\@supervisoronename\unskip,}
  {\noindent\@supervisoroneaffiliation\unskip\par}
  \ifthenelse{\equal{\@supervisortwoname}{}}{}{
    {\noindent\bfseries\@supervisortwoname\unskip,}
    {\noindent\@supervisortwoaffiliation\unskip\par}
  }
  \ifthenelse{\equal{\@supervisorthreename}{}}{}{
    {\noindent\bfseries\@supervisorthreename\unskip,}
    {\noindent\@supervisorthreeaffiliation\unskip\par}
  }
  \ifthenelse{\equal{\@supervisorfourname}{}}{}{
    {\noindent\bfseries\@supervisorfourname\unskip,}
    {\noindent\@supervisorfouraffiliation\unskip\par}
  }
  \ifthenelse{\equal{\@supervisorfivename}{}}{}{
    {\noindent\bfseries\@supervisorfivename\unskip,}
    {\noindent\@supervisorfiveaffiliation\unskip\par}
  }

\ifthenelse{\equal{\@institutionlogo}{}}{}{
  \begin{tikzpicture}[remember picture,overlay,shift={(current page.south east)}]
    \node[anchor=south east,xshift=-2cm,yshift=2cm]{\includegraphics[width=\@institutionlogowidth]{\@institutionlogo}};
  \end{tikzpicture}
}

%\vfill

\end{titlepage}
}
\makeatother

\endinput