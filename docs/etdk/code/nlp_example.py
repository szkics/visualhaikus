import spacy
import io
import sys

nlp = spacy.load('en_core_web_sm')
ff = io.open(sys.argv[1], 'r', encoding='utf-8')
doc = nlp(ff.read())

dictionaryOfNouns = []
for sent in doc.sents:
    for token in sent:
        if (token.pos_ == "NOUN"):
            for child in token.children:
                if (child.pos_ == "ADJ"): 
                    tuple = child.text, token.text
                    dictionaryOfNouns.append(tuple)
print(dictionaryOfNouns)