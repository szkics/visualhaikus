# LaTeX példa ETDK cikkre #

Ez a könyvtár egy példát nyújt ETDK dolgozat elkészítésére LaTeX segítségével. Használható:
- Overleaf-en - [hivatalos verzió](https://www.overleaf.com/read/sjvjntngfzgp)
- lokálisan

## Lokális PDF generálás ##

Lokális generálásra szükség van egy LaTeX fordítóra (ajánljuk a [MiKTeX](https://miktex.org/download) vagy [TeXLive](https://www.tug.org/texlive/quickinstall.html) fordítókat) és a [Biber](http://biblatex-biber.sourceforge.net/) könyvészetkezelő könyvtárra (TeXLive használata esetén ez `tlmgr install biber` segítségével telepíthető).

Megfelelő eszközök jelenlétében a PDF a következő parancssorral generálható le:

```
pdflatex -shell-escape document.tex
biber document
pdflatex -shell-escape document.tex
```
