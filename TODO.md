# clean code.
	Write beautiful code, each function in the corresponding file.
	Source code copied from others should be referenced. Add license if nedded.
	https://www.pyimagesearch.com/2018/05/07/multi-label-classification-with-keras/
	"Sure, feel free to use the code. I kindly ask that you give credit and include a link back to the PyImageSearch site in the README. 
	If you can do that I would really appreciate it 🙂 Having a link back really helps me out. Thank you!" - Adrian Rosebrock 

# start writing for real.
	Write about the terms of:
		* ekphrasis
		* haiku
		* landscape
		* painting
		* visual data
		* artificial intelligence
		* tensor
		* tensorflow
		* keras
		* neural network
		* perceptron
		* computational poetry
		* digital poetry
		* natural language processing
		* spaCy
		* the whole idea of the project
		* steps of writing the project (look up previous git commits).
		* intersection of art and computer science.
		* python, python 3.6.
