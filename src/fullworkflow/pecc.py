import spacy
import io
import sys
import operator

nlp = spacy.load('en_core_web_sm')
f = io.open(sys.argv[1], 'r', encoding='utf-8')
doc = nlp(f.read())

verbFrequency = dict()
for sent in doc.sents:
    for token in sent:
        if (token.pos_ == "VERB"):
            if ((token.text).lower() in verbFrequency.keys()):
                verbFrequency[(token.text).lower()] += 1
            else:
                verbFrequency[(token.text).lower()] = 1
verbFreqList = sorted(verbFrequency.items(),key=operator.itemgetter(1),reverse=True)
maximum = verbFreqList[0][1]
frequentVerbs = []
for verbtuple in verbFreqList:
    if (verbtuple[1] >= int(maximum/4) and verbtuple[1] <= int(maximum/2)): # TODO: a general method for these interval bounds. why not between 15 and 20? find a correlation between the number of pages (length of the dictionary) and these numbers.
        frequentVerbs.append(verbtuple)

print(frequentVerbs,file=open("list_of_frequent_verbs.txt","w"))
# print(frequentVerbs)