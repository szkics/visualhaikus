import random

# 	generative grammar of a haiku:
#		S --> NV | N | V
#		N --> adj noun | noun 
#		V --> verb adv | verb 

# each symbol on the left of the arrows is represented by a list of rules.
# for example, the 'N' rule is a list of rules: ['AN','AN','AN','AN','AN','AN','N','N','N','N'] consisting of 6 'AN'-s and 4 'N'-s.
# a list was chosen to represent the weights of each possible pattern.
# random.choice(N) picks an element from the N list randomly.
def formula():
	
	grammar = {	'S': {
					'NV': 6,
					'N': 3, 
					'V': 1 },
				
				'N': {
					'AN': 6,
					'N' : 4  
				},

				'V': {
					'VA': 7,
					'V': 3
				}
	}

	listOfLists = []
	for symbol in grammar:
		pattern = grammar[symbol]
		for word in pattern:
			listOfLists.append([word]*pattern[word])

	S = listOfLists[0] + listOfLists[1] + listOfLists[2]
	N = listOfLists[3] + listOfLists[4]
	V = listOfLists[5] + listOfLists[6]

	output = ''
	start = random.choice(S)
	if (start == 'NV'):
		output = output + random.choice(N)
		output = output + random.choice(V)
	elif (start == 'N'):
		output = output + random.choice(N)
	else:
		output = output + random.choice(V)	
	
	return output

print(formula())