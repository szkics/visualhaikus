from keras.preprocessing.image import img_to_array
from keras.models import load_model
import numpy as np
import argparse
import imutils
import pickle
import cv2
import os
import sys
import ast
import random
from generative_grammar import formula

def conjugate(verb):
    # Verbs ending with consonant and “y” change “y” for “ies”
    vowels = 'aeiou'
    # Verbs ending with “o”
    if (len(verb) >=2):
	    if (verb[-1] == 'o'):
	        verb = verb + 'es'
	    # Verbs ending in sibilant sounds –s, -z, -ch, -sh, or -x add “es” to infinitive
	    elif (verb[-1] == 's' or verb[-1] == 'z' or verb[-1] == 'x' or (verb[-2] == 'c' and verb[-1] == 'h') or (verb[-2] == 's' and verb[-1] == 'h')):
	        verb = verb + 'es'
	    # if the character before the last character is consonant and the last char is y.
	    elif ((verb[-2] not in vowels) and (verb[-1] == 'y')):
	        verb = verb[:-1] + 'ies'
	    elif (verb == 'have'):
	        verb = 'has'
	    elif (verb == 'may'):
	        verb = 'may'
	    # the most general case:
	    else:
	        verb = verb + 's'
    return verb

# https://stackoverflow.com/questions/46759492/syllable-count-in-python
def numberOfSyllables(word):
    word = word.lower()
    count = 0
    vowels = "aeiouy"
    if word[0] in vowels:
        count += 1
    for index in range(1, len(word)):
        if word[index] in vowels and word[index - 1] not in vowels:
            count += 1
    if word.endswith("e"):
        count -= 1
    if count == 0:
        count += 1
    return count

def getChildNode(word, path):
        with open(path, 'r') as f:
                listOfTuples = ast.literal_eval(f.read())
        bestChoice = 0
        selectedChild = ""
        for child, parent in listOfTuples:
                if parent == word:
                        actualNumberOfSyllables = numberOfSyllables(parent) + numberOfSyllables(child)
                        if actualNumberOfSyllables > bestChoice:
                                bestChoice = actualNumberOfSyllables
                                selectedChild = child
        return selectedChild

def getArticle(noun):
    vowels = 'aeiou'
    if (len(noun) > 0):
        if (random.random() > 0.5):
            if (noun[0] in vowels):
            	article = "an"
            else:
            	article = "a"
        else:
        	article = "the"
    else:
        article = "the"
    return article

def getLabel(noun,verb):
	adj = getChildNode(noun,'./adj_noun_dict.txt')
	adv = getChildNode(verb,'./adv_verb_dict.txt')

	if 	(formula() == 'ANVA'):
	    label = getArticle(adj) + " " + adj + " " + noun + " " + adv + " " + conjugate(verb)
	elif (formula() == 'ANV'):
	    label = getArticle(adj) + " " + adj + " " + noun + " " + conjugate(verb)
	elif (formula() == 'NVA'):
	    label = getArticle(noun) + " " + noun + conjugate(verb) + adv
	elif (formula() == 'NV'):
	    label = getArticle(noun) + " " + noun + " " + conjugate(verb)
	elif (formula() == 'AN'):
	    label = getArticle(adj) + " " + adj + " " + noun
	elif (formula() == 'N'):
	    label = getArticle(noun) + " " + noun
	elif (formula() == 'VA'):
	    label = conjugate(verb) + " " + adv
	else:
	    label = conjugate(verb)

	return label

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-m", "--model", required=True,
	help="path to trained model model")
ap.add_argument("-l", "--labelbin", required=True,
	help="path to label binarizer")
ap.add_argument("-i", "--image", required=True,
	help="path to input image")
args = vars(ap.parse_args())
# Three parameters when called are:
    # 1. --model: path to trained model
    # 2. --labelbin: path to label binarizer file
    # 3. --image: input image path

# load the image
image = cv2.imread(args["image"])
output = image.copy()
print('Original Dimensions : ',image.shape)
scale_percent = 30 # percent of original size
# width = int(image.shape[1] * scale_percent / 100)
# height = int(image.shape[0] * scale_percent / 100)
width = int((image.shape[1] * 750) /  image.shape[0])
height = 750
dim = (width, height)

# https://www.tutorialkart.com/opencv/python/opencv-python-resize-image/
# resize image
resized = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
print('Resized Dimensions : ',resized.shape)

# pre-process the image in the exact same manner as training
image = cv2.resize(image, (96, 96))
image = image.astype("float") / 255.0
image = img_to_array(image)
image = np.expand_dims(image, axis=0)


# load the trained convolutional neural network and the label binarizer
print("[INFO] loading network...")
model = load_model(args["model"])
lb = pickle.loads(open(args["labelbin"], "rb").read())

# classify the input image
print("[INFO] classifying image...")
proba = model.predict(image)[0]

with open('list_of_ham_verbs.txt', 'r') as f:
        listOfTuples = ast.literal_eval(f.read())

threeVerbs = []
while (len(threeVerbs) < 3):
    randomWordFromList = random.choice(listOfTuples)
    listOfTuples.remove(randomWordFromList)
    threeVerbs.append(randomWordFromList[0])
print(threeVerbs)
# show the probabilities for each of the individual labels
sys.stdout=open("probs.txt","w")
for (label, p) in zip(lb.classes_, proba):
	print("{}: {:.2f}%".format(label, p * 100))
sys.stdout.close()

filepath = 'probs.txt'
probs = []
nouns = []
with open(filepath) as fp:
        for line in fp:
        		# print(line)
        		count = 0
        		for word in line.split(':'):
        			if (count == 1):
        				probs.append(int(float(word[:-2][1:])*100))
        			else:
        				nouns.append(word)
        			count = count + 1
fp.close()

zipped = list(zip(nouns,probs))
sortedList = sorted(zipped, key = lambda x: x[1], reverse=True)
nouns = [i[0] for i in sortedList[:3]]
# preprocess nouns, so they would all have singular form.

vowels = 'aeiou'
for i in (range(len(nouns))):
	if (nouns[i][-1] == 's' and nouns[i][-2] == 'e'):
		if (nouns[i][-3] in vowels):
			nouns[i] = nouns[i][:-3] + 'y'
		else:
			nouns[i] = nouns[i][:-1]
	elif (nouns[i][-1] == 's'):
		nouns[i] = nouns[i][:-1]

text = ""
for i in range(0,len(nouns)):
        #print(nouns[i])
        label = getLabel(nouns[i],threeVerbs[i])
        # https://gist.github.com/aplz/fd34707deffb208f367808aade7e5d5c
        label = label.lstrip() # this is to clear beginning of string if something is not found. (although it always should be found)
        font_scale = 1
        font = cv2.FONT_HERSHEY_DUPLEX # cv2.FONT_HERSHEY_PLAIN
        (text_width, text_height) = cv2.getTextSize(label, font, fontScale=font_scale, thickness=1)[0]
        rectangle_bgr = (255, 255, 255)
        text_offset_x = 10
        text_offset_y = (i * 38) + 25
        box_coords = ((text_offset_x, text_offset_y + 13), (text_offset_x + text_width + 2, text_offset_y - text_height - 2))
        cv2.rectangle(resized, box_coords[0], box_coords[1], rectangle_bgr, cv2.FILLED)

        cv2.putText(resized, label, (text_offset_x, text_offset_y), font, fontScale=font_scale, color=(0, 0, 0), thickness=1)
cv2.imshow("Resized image", resized)
cv2.waitKey(0)
