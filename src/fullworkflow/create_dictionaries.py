from __future__  import unicode_literals
import spacy
import io
import sys
import operator

# checking number of arguments and showing correct usage. 
if (len(sys.argv) != 2):
    print('Usage: python3.6 create_dictionaries.py [input_file.txt]')
    sys.exit()

# checking if input is truly a .txt file.
if (sys.argv[1].endswith('.txt')):
    print('* Started processing input text. *')
    print('* A few seconds and the dictionaries consisting of (adverb,verb) and (adj,noun) tuples will be ready to use. *')
    nlp = spacy.load('en_core_web_sm')

    ff = io.open(sys.argv[1], 'r', encoding='utf-8')
    dokk = nlp(ff.read())

    dictionaryOfNouns = []
    dictionaryOfVerbs = []
    verbFrequency = dict()

    # iterating once over the full input text so thanks to POS tagging 
    # (adjective, noun) and (adverb, verb) tuples can be collected.
    # in addition verb frequency is also examined, to decide which verbs 
    # are good to use in a poem. 
    for sent in dokk.sents:

        for token in sent:
            
            if (token.pos_ == "NOUN"):
                for child in token.children:
                    if (child.pos_ == "ADJ"): 
                        tuple = child.lemma_, token.lemma_
                        dictionaryOfNouns.append(tuple)
            
            if (token.pos_ == "VERB"):
                
                if (token.lemma_ in verbFrequency.keys()):
                    verbFrequency[token.lemma_] += 1
                else:
                    verbFrequency[token.lemma_] = 1
                
                for child in token.children:
                    if (child.pos_ == "ADV"):     
                        tuple = child.lemma_, token.lemma_
                        dictionaryOfVerbs.append(tuple)

    print(dictionaryOfNouns,file=open("adj_noun_dict.txt","w"))
    print(dictionaryOfVerbs,file=open("adv_verb_dict.txt","w"))
    print('* The dictionaries were created successfully. *')

    # an elegant way of sorting elements of a dict in a descending order based on values
    # and making a list of tuples out of them. 
    verbFreqList = sorted(verbFrequency.items(),key=operator.itemgetter(1),reverse=True)
    print(verbFreqList[0][1])
    maximum = verbFreqList[0][1]
    selectedlist = []
    for verbtuple in verbFreqList:
        if (verbtuple[1] >= int(maximum/4) and verbtuple[1] <= int(maximum/2)): # TODO: a general method for these interval bounds. why not between 15 and 20? find a correlation between the number of pages (length of the dictionary) and these numbers.
            selectedlist.append(verbtuple)

    print(selectedlist,file=open("list_of_ham_verbs.txt","w"))
    print('* The verb frequency is examined and useful verbs for the haiku are collected. *')

    ff.close()
else:
    print('Input file must have .txt format.')
