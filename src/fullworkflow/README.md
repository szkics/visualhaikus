The Visual Haikus project consists of the following steps:

1. the natural language processing part of the project is in fact the production of an association dictionary with the help of a python library named spacy. In fact, two dictionaries, one which consists of (adjective, noun) pairs, and another of (adverb, verb) pairs. These tuples will have key role in writing poems inspired by an images, as the backbone of the haiku will be composed by them.  

For this to happen type the following in your terminal:

	python3.6 create_dictionaries.py the_japanese_quince.txt

If you do not have the necessary packages, install them by:
	
	sudo python3.6 -m pip install spacy

To download the used language model:

	python3.6 -m spacy download en_core_web_sm

2. the image classification part of the project is where from 20 nature related classes (birds, tree, cloud, etc.) a neural network model is built up and trained, from which using keras we could classify a never seen before image and decide which of the potential classes are represented. For this: 

a. download a dataset using bing web search api. (https://azure.microsoft.com/en-us/services/cognitive-services/bing-web-search-api/)

	python3.6 search_bing_api.py --query "searched word" --output dataset/searched_word
where dataset/ is the folder in which there are images in subfolders named after the class they represent. in this case searched_word is the name of the subfolder in which there will be pictures downloaded from bing after typing: "searched word". 

b. train the network.

	python3.6 train.py --dataset dataset --model haiku.model --labelbin lb.pickle

c. classify the given image.
	
	python3.6 classify.py --model haiku.model  --labelbin lb.pickle --image path/to/im.jpg

3. after the image is classified, nouns which the CNN affirms to be present in the input are searched for in the dictionary of associations, and when found the noun will have an adjective assigned to it. The verbs are associated to the nouns based on the examination of verb frequency. The free haiku form is generated by a set of generative grammar rules.

Huge thanks to Adrian Rosebrock, the part with image classification is based on his code: 
	https://www.pyimagesearch.com/2018/05/07/multi-label-classification-with-keras/