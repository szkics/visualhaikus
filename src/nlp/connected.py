import sys
import ast
import random 

def numberOfSyllables(word):
    word = word.lower()
    count = 0
    vowels = "aeiouy"
    if word[0] in vowels:
        count += 1
    for index in range(1, len(word)):
        if word[index] in vowels and word[index - 1] not in vowels:
            count += 1
    if word.endswith("e"):
        count -= 1
    if count == 0:
        count += 1
    return count

def listIterator(word):
	with open('../../data/dict3.txt', 'r') as f:
		listOfTuples = ast.literal_eval(f.read())

    	bestChoice = 0
    	for adj,noun in listOfTuples:
    		if noun == word:
        		actualNumberOfSyllables = numberOfSyllables(noun) + numberOfSyllables(adj)
    			if actualNumberOfSyllables > bestChoice:
                		bestChoice = actualNumberOfSyllables
                		selectedNoun = noun
                		selectedAdj = adj
	return selectedAdj, selectedNoun

with open('listOfHamVerbs.txt', 'r') as f:
	listOfTuples = ast.literal_eval(f.read())

threeVerbs = []
for verb,numberOfOccurences in listOfTuples:
	if ((random.random() <= 0.2) & (len(threeVerbs) < 3)):
#		print verb
		threeVerbs.append(verb)

#print threeVerbs

adj, noun = listIterator('shadow')
print adj + " " + noun + " " + threeVerbs[0]
adj, noun = listIterator('water')
print adj + " " + noun + " " + threeVerbs[1]
adj, noun = listIterator('sun')
print adj + " " + noun + " " + threeVerbs[2]


# a huge shadow grows. / cries.
# under motionless water
# there's a brighter sun.
