from __future__  import unicode_literals
import spacy
import io
import matplotlib.pyplot as plt

nlp = spacy.load('en_core_web_sm')

ff = io.open('../../data/input4.txt', 'r', encoding='utf-8')
dokk = nlp(ff.read())
verbdict = dict()
for sent in dokk.sents:
	for token in sent:
		if (token.pos_ == "VERB"):
			if ((token.text).lower() in verbdict.keys()):
				verbdict[(token.text).lower()] += 1
			else:
				verbdict[(token.text).lower()] = 1

verblist = list(verbdict.items())

for verbs in range(len(verblist)-1, -1, -1):
	swapped = False
	for i in range(verbs):
		if verblist[i][1] < verblist[i+1][1]:
			verblist[i], verblist[i+1] = verblist[i+1], verblist[i]
			swapped = True
	if not swapped:
		break

selectedlist = []
for verbtuple in verblist:
	if (verbtuple[1] >= 15 and verbtuple[1] <= 20):
		selectedlist.append(verbtuple)

with open('listOfHamVerbs.txt', 'w') as f:
	print >> f, selectedlist

plt.bar(range(len(selectedlist)), [val[1] for val in selectedlist], align='center')
plt.xticks(range(len(selectedlist)), [val[0] for val in selectedlist])
plt.xticks(rotation=70)
plt.show()

with open('../../data/verbdict4.txt', 'w') as f:
        print >> f, verblist

#print("Verbs:", [token.lemma_ for token in doc if token.pos_ == "VERB"])
