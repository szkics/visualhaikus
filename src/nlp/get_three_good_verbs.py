import ast
import random

with open('listOfHamVerbs.txt', 'r') as f:
	listOfTuples = ast.literal_eval(f.read())

threeVerbs = []
for verb,numberOfOccurences in listOfTuples:
	if ((random.random() <= 0.2) & (len(threeVerbs) < 3)):
		print verb
		threeVerbs.append(verb)

print threeVerbs
