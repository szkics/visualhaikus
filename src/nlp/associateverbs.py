from __future__  import unicode_literals
import spacy
import io

nlp = spacy.load('en_core_web_sm')

ff = io.open('../../data/input4.txt', 'r', encoding='utf-8')
dokk = nlp(ff.read())
dictionary = []
for sent in dokk.sents:
    for token in sent:
        if (token.pos_ == "VERB"):
                lastNoun = token.text
                for child in token.children:
                        if (child.pos_ == "ADV"):     
                                tuple = child.text, token.text
                                dictionary.append(tuple) 

with open('../../data/adv_verb4.txt', 'w') as f:
        print >> f, dictionary

ff.close()

