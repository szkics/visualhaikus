from __future__  import unicode_literals
import spacy
import io

nlp = spacy.load('en_core_web_sm')
#doc = nlp(u'The weather is beautiful.')
#doc2 = nlp(u'The beautiful weather makes me feel good.')
#doc3 = nlp(u'The misty mountains are foggy.')
#for token in doc:
#	print(token.text,"---->", token.pos_ ,"---->",token.dep_)

#print("------------------------------------")

#for token in doc:
#	for child in token.children:
#		print (token.text, "children:", child.text)

#print("--------------------------------")

ff = io.open('../../data/input4.txt', 'r', encoding='utf-8')
dokk = nlp(ff.read())
dictionary = []
for sent in dokk.sents:
    for token in sent:
        if (token.pos_ == "NOUN"):
    		lastNoun = token.text
    		for child in token.children:
    			if (child.pos_ == "ADJ"): # this only filters amod.
                    		tuple = child.text, token.text
                    		dictionary.append(tuple)
                    #print(child.text,"-->",token.text)
    	if (token.pos_ == "VERB"):
    		for child in token.children:
    			 if (child.dep_ == "acomp"): #this sometimes (doc2) is not correct, although we should consider to use it for other cases. otherwise, doc3 would not find (foggy, mountains) as (ADJ, NOUN) pair
                    		tuple = child.text, lastNoun
                    		dictionary.append(tuple)
                    # print(child.text,"-->",lastNoun)

#print(dictionary)
with open('../../data/dict4.txt', 'w') as f:
	print >> f, dictionary

ff.close()
