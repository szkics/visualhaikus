from keras.preprocessing.image import img_to_array
from keras.models import load_model
import numpy as np
import argparse
import imutils
import pickle
import cv2
import os
import sys

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-m", "--model", required=True,
	help="path to trained model model")
ap.add_argument("-l", "--labelbin", required=True,
	help="path to label binarizer")
ap.add_argument("-i", "--image", required=True,
	help="path to input image")
args = vars(ap.parse_args())
# Three parameters when called are:
    # 1. --model: path to trained model
    # 2. --labelbin: path to label binarizer file
    # 3. --image: input image path

# load the image
image = cv2.imread(args["image"])
output = image.copy()

# pre-process the image in the exact same manner as training
image = cv2.resize(image, (96, 96))
image = image.astype("float") / 255.0
image = img_to_array(image)
image = np.expand_dims(image, axis=0)

# load the trained convolutional neural network and the label binarizer
print("[INFO] loading network...")
model = load_model(args["model"])
lb = pickle.loads(open(args["labelbin"], "rb").read())

# classify the input image
print("[INFO] classifying image...")
proba = model.predict(image)[0]
idxs = np.argsort(proba)[::-1][:3]

for (i, j) in enumerate(idxs):
	# build the label and draw the label on the image
	label = "{}: {:.2f}%".format(lb.classes_[j], proba[j] * 100)
	cv2.putText(output, label, (10, (i * 30) + 25),
		cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 0), 2)

# show the probabilities for each of the individual labels
sys.stdout=open("probs.txt","w")
for (label, p) in zip(lb.classes_, proba):
	print("{}: {:.2f}%".format(label, p * 100))
sys.stdout.close()

# show the output image
cv2.imshow("Output", output)
cv2.waitKey(0)

# Terminal Command to run on an example:
# python classify.py --model <MODEL_NAME>.model --labelbin lb.pickle --image examples/<FILE_NAME.jpg>
# image exits on the press of a key

