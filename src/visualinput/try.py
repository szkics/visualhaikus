import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import tensorflow as tf 
import matplotlib.pyplot as plt
import keras.backend as K
import numpy as np

print(tf.__version__)


mnist = tf.keras.datasets.mnist # 28 x 28 images of hand-written digits from 0 to 9. 
(x_train, y_train), (x_test, y_test) = mnist.load_data()

#the values are scaled between 0 and 1. 
x_train = tf.keras.utils.normalize(x_train, axis=1)
x_test = tf.keras.utils.normalize(x_test, axis=1) 

#print(x_train[0])

#creating the model.
model = tf.keras.models.Sequential()
#the first layer, the input layer:
model.add(tf.keras.layers.Flatten())
#hidden layers. in this case there are 2 hidden layers both with 128 neurons.
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
#finally the output layer, the 10 digits (classes).
model.add(tf.keras.layers.Dense(10, activation=tf.nn.softmax))


#training the model. 
# loss --> the degree of error, the goal is to minimize loss.
model.compile(optimizer='adam',loss='sparse_categorical_crossentropy',metrics=['acc'])
model.fit(x_train,y_train,epochs=3)

# calculating validation loss and validation accuracy.
val_loss, val_acc = model.evaluate(x_test, y_test) 
print(val_loss, val_acc)

model.save('OCR.model')

new_model = tf.keras.models.load_model('OCR.model')
predictions = new_model.predict([x_test])
print(predictions)

print(np.argmax(predictions[0]))

plt.imshow(x_test[0])
plt.show()

