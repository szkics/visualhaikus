import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import cv2
import tensorflow as tf

CATEGORIES = ["grass", "cloud"] # river, mountain, leaves, stars, sky, sun, moon, flower, pine, tree


def prepare(filepath):
    IMG_SIZE = 100  # 50 in txt-based
    img_array = cv2.imread(filepath, cv2.IMREAD_GRAYSCALE)
    img_array = img_array/255.0
    new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
    return new_array.reshape(-1, IMG_SIZE, IMG_SIZE, 1)


model = tf.keras.models.load_model("64x3-CNN.model")

prediction = model.predict([prepare('grass.jpg')])
print(prediction)  # will be a list in a list.
print(prediction[0][0])
if (prediction[0][0] > 0.5):
	print(CATEGORIES[1])
else:
	print(CATEGORIES[0])

print(CATEGORIES[int(prediction[0][0])])

predictionProbabilities = model.predict_proba([prepare('grass.jpg')])
print(predictionProbabilities)

predictionClasses = model.predict_classes([prepare('grass.jpg')])
print(predictionClasses)
