modelling a perceptron, which is similar to a singular neuron.
it has inputs, 
	i -- \
	i -- -\
	i -- -/
	i -- /
which are connected by weights to the neuron, the sum of these inputs, thus calculating the output.

the whole concept is to get close to these outputs: 
	[0,1,1,0]
the final result after training:
	[[0.00679672]
 	[0.99445583]
 	[0.99548516]
 	[0.00553614]]
	
