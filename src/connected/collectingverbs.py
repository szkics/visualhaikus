from __future__  import unicode_literals
import spacy
import io
import matplotlib.pyplot as plt

nlp = spacy.load('en_core_web_sm')

ff = io.open('../../data/input4.txt', 'r', encoding='utf-8')
dokk = nlp(ff.read())
verbFrequency = dict()
for sent in dokk.sents:
	for token in sent:
		if (token.pos_ == "VERB"):
			if ((token.text).lower() in verbFrequency.keys()):
				verbFrequency[(token.text).lower()] += 1
			else:
				verbFrequency[(token.text).lower()] = 1

verbFreqList = list(verbFrequency.items())

for verbs in range(len(verbFreqList)-1, -1, -1):
	swapped = False
	for i in range(verbs):
		if verbFreqList[i][1] < verbFreqList[i+1][1]:
			verbFreqList[i], verbFreqList[i+1] = verbFreqList[i+1], verbFreqList[i]
			swapped = True
	if not swapped:
		break

selectedlist = []
for verbtuple in verbFreqList:
	if (verbtuple[1] >= 0 and verbtuple[1] <= 30): # TODO: a general method for these interval bounds. why not between 15 and 20? find a correlation between the number of pages (length of the dictionary) and these numbers.
		selectedlist.append(verbtuple)

with open('listOfHamVerbs.txt', 'w') as f:
	print >> f, selectedlist

plt.bar(range(len(selectedlist)), [val[1] for val in selectedlist], align='center')
plt.xticks(range(len(selectedlist)), [val[0] for val in selectedlist])
plt.xticks(rotation=70)
plt.show()

with open('../../data/verbFrequency4.txt', 'w') as f:
        print >> f, verbFreqList

#print("Verbs:", [token.lemma_ for token in doc if token.pos_ == "VERB"])
