from keras.preprocessing.image import img_to_array
from keras.models import load_model
import numpy as np
import argparse
import imutils
import pickle
import cv2
import os
import sys
import ast
import random
from generative_grammar import formula

# https://stackoverflow.com/questions/46759492/syllable-count-in-python 
def numberOfSyllables(word):
    word = word.lower()
    count = 0
    vowels = "aeiouy"
    if word[0] in vowels:
        count += 1
    for index in range(1, len(word)):
        if word[index] in vowels and word[index - 1] not in vowels:
            count += 1
    if word.endswith("e"):
        count -= 1
    if count == 0:
        count += 1
    return count

def listIterator(word, path):
        with open(path, 'r') as f:
                listOfTuples = ast.literal_eval(f.read())
        bestChoice = 0
        selectedAdj = ""
        selectedNoun = ""
        for adj,noun in listOfTuples:
                if noun == word:
                        actualNumberOfSyllables = numberOfSyllables(noun) + numberOfSyllables(adj)
                        if actualNumberOfSyllables > bestChoice:
                                bestChoice = actualNumberOfSyllables
                                selectedNoun = noun
                                selectedAdj = adj
                                #print(selectedAdj + " " + selectedNoun)
        return selectedAdj, selectedNoun

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-m", "--model", required=True,
	help="path to trained model model")
ap.add_argument("-l", "--labelbin", required=True,
	help="path to label binarizer")
ap.add_argument("-i", "--image", required=True,
	help="path to input image")
args = vars(ap.parse_args())
# Three parameters when called are:
    # 1. --model: path to trained model
    # 2. --labelbin: path to label binarizer file
    # 3. --image: input image path

# load the image
image = cv2.imread(args["image"])
output = image.copy()

# pre-process the image in the exact same manner as training
image = cv2.resize(image, (96, 96))
image = image.astype("float") / 255.0
image = img_to_array(image)
image = np.expand_dims(image, axis=0)

# load the trained convolutional neural network and the label binarizer
print("[INFO] loading network...")
model = load_model(args["model"])
lb = pickle.loads(open(args["labelbin"], "rb").read())

# classify the input image
print("[INFO] classifying image...")
proba = model.predict(image)[0]

with open('listOfHamVerbs.txt', 'r') as f:
        listOfTuples = ast.literal_eval(f.read())

threeVerbs = []
for verb,numberOfOccurences in listOfTuples:
        if ((random.random() <= 0.4) & (len(threeVerbs) < 3)): # TODO: here a little bit smarter solution
#               print verb
                threeVerbs.append(verb)

# show the probabilities for each of the individual labels
sys.stdout=open("probs.txt","w")
for (label, p) in zip(lb.classes_, proba):
	print("{}: {:.2f}%".format(label, p * 100))
sys.stdout.close()

filepath = 'probs.txt'
L = []
with open(filepath) as fp:
        for line in fp:
                for word in line.split(): 
                        L.append(word)
fp.close()

nouns = []
for i in range(0, len(L), 2):
        #print(L[i+1][:-1])
        if float(L[i+1][:-1]) > 14:
                #print(L[i][:-1])
                nouns.append(L[i][:-1])
#print(nouns)
concatenate = ""
goodVerbs = ['followed','disappeared','answered']
for i in range(0,len(nouns)):
        #print(nouns[i])
        adj, noun = listIterator(nouns[i],'../../data/dict4.txt')
       	adv, verb = listIterator(threeVerbs[i],'../../data/adv_verb4.txt') 

        if (formula() == 'ANVA'):
            label = adj + " " + noun + " " + adv + " " + verb
        elif (formula() == 'ANV'):
            label = adj + " " + noun + " " + verb
        elif (formula() == 'NVA'):
            label = noun + " " + adv + " " + verb
        elif (formula() == 'NV'):
            label = noun + " " + verb
        elif (formula() == 'AN'):
            label = adj + " " + noun
        elif (formula() == 'N'):
            label = noun
        elif (formula() == 'VA'):
            label = adv + " " + verb
        else:
            label = verb

        cv2.putText(output, label, (10, (i * 30) + 25),
			cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 0), 2)


# show the output image
cv2.imshow("Output", output)
cv2.waitKey(0)
# Terminal Command to run on an example:
# python classify.py --model <MODEL_NAME>.model --labelbin lb.pickle --image examples/<FILE_NAME.jpg>
# image exits on the press of a key

