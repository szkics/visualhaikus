import sys
import ast
import random 

def numberOfSyllables(word):
    word = word.lower()
    count = 0
    vowels = "aeiouy"
    if word[0] in vowels:
        count += 1
    for index in range(1, len(word)):
        if word[index] in vowels and word[index - 1] not in vowels:
            count += 1
    if word.endswith("e"):
        count -= 1
    if count == 0:
        count += 1
    return count

def listIterator(word):
	with open('../../data/dict4.txt', 'r') as f:
		listOfTuples = ast.literal_eval(f.read())
	bestChoice = 0
	selectedAdj = "ftw"
	selectedNoun = "wtf"
	listOfAssociations = []
	for adj,noun in listOfTuples:
    		if noun == word:
        		actualNumberOfSyllables = numberOfSyllables(noun) + numberOfSyllables(adj)
        		if actualNumberOfSyllables > bestChoice:
        			bestChoice = actualNumberOfSyllables
        			selectedNoun = noun
        			selectedAdj = adj
        			#print(selectedAdj + " " + selectedNoun)
             listOfAssociations.append((selectedNoun, selectedAdj))
	print listOfAssociations
	return selectedAdj, selectedNoun

with open('listOfHamVerbs.txt', 'r') as f:
	listOfTuples = ast.literal_eval(f.read())

threeVerbs = []
for verb,numberOfOccurences in listOfTuples:
	if ((random.random() <= 0.4) & (len(threeVerbs) < 3)): # TODO: here a little bit smarter solution
#		print verb
		threeVerbs.append(verb)

#print threeVerbs
filepath = 'probs.txt'
L = []
with open(filepath) as fp:
        for line in fp:
                for word in line.split():
                        L.append(word)
fp.close()
#print(L)
nouns = []
for i in range(0, len(L), 2):
        #print(L[i+1][:-1])
        if (float(L[i+1][:-1]) > 10) and float(L[i+1][:-1]) < 13)):
                #print(L[i][:-1])
                nouns.append(L[i][:-1])
#print(nouns)
for i in range(0,len(nouns)):
	#print(nouns[i])
	adj, noun = listIterator(nouns[i])
	print(adj + " " + noun + " " + threeVerbs[i])

