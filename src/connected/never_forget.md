when using python, just remember:
	
	sudo python3.6 -m pip install matplotlib (name of the package)
	
	if a problem occurs, it's probably because there is a clash with a previously installed version of the certain package.
so just type in the following:
	
	sudo aptitude search scipy
	sudo apt-get remove python3-scipy

	and ffs please just use python3.6 and do not install 10 versions of python nor pip.

a very useful link:
https://www.pyimagesearch.com/2018/05/07/multi-label-classification-with-keras/		
