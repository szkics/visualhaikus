# Visual Haikus

Briefly the project is to write a program which writes poems from a landscape, or any given picture. 
The program would be inspired, like a real poet by what it sees and observes in its surroundings.
We show it a visual segment of life, and from its emotional experience (well, from reading a few pages of literature) we get a written segment of life.
 
The project would mainly use two pyhton libraries: 
	spaCy, which offers amazing opportunities to develop the linguistics aspects of the project.
	tensorFlow, which provides the possibility of image recognition and classification. 

# 1. Firstly the task is to collect (adjective, noun) tuples (from any given text), as these would represent the core of an abstract poem.

# 2. Afterwards: iterate through the dictionary and extract the n given words: for example, 
	let n = 3 
	and the n words: mountain, cloud, sky.
	The function could potentially find these tuples:
	(misty, mountain) 4 
	(fluffy, cloud) 3
	(wandering, sky) 4

# 3. The haiku form is 
	5 
	7 
	5
	We could make the base of the poem 4 3 4 -> 5 7 5 by using stop words and verbs. 
	For example:
	
		the misty mountain 5 
		breathes in a fluffy cloud. the 7
		sky is wandering. 5
	
	(When the number of syllables is 3 or 4, it could be a general hack to make {fluffy cloud} (3) into {the cloud is/was fluffy.}
		Or with a break: .... the / sky is wandering, like in our example.)

# 4. To spice things up these n words could be collected from a picture, using image recognition and python's tensorflow library.
In general to illustrate the abilities of tensorFlow examples such as car, human, building are used.
In this project it could be considered using elements of a landscape:
	trees: pine, beech, willow, oak.
	sun, moon, sky, cloud, fog. 
	field, grass.
	flowers: poppy, tulip, lily, sunflower.
	mountain, peak.
	ocean, river, sea, lake. 

5. Some pictures of the project:
	
	* the graph of verb frequency of Hemingway's Old Man and the Sea:

	![alt text](graph.png?raw=true "Ham verbs of Hemingway")

	* classifying objects of a landscape:

	![alt text](classification.png?raw=true "classifying almost correctly")

	* some interesting results conceived by the intersection of words from The Last Of The Mohicans by James Fenimore Cooper and a photo of birds and cliffs in fog.

	![alt text](ontheway.png?raw=true "one of the first results")

	* another rawer result, this is the outcome before making a haiku out of the text: 

	![alt text](before.png?raw=true "before")

	* during the process such poems were born, it could even have a title: momentarily forgotten, the poem which masters gradation by repetition: 

	![alt text](during.png?raw=true "during")

	* outcomes:

	![alt text](after1.png?raw=true "after1")

	![alt text](after2.png?raw=true "after2")

	* two beautiful poems about clouds and birds:

	![alt text](birds.png?raw=true "birds")

	![alt text](cloud.png?raw=true "cloud")
